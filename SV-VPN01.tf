module "instance" {
  source = "git::https://github.com/James-kevin/aws-module-vm-linux.git"

  aws_region    = "eu-north-1"
  vpc_id        = "vpc-0d4fc944cc5eff31e"
  ami_id        = "ami-0989fb15ce71ba39e" #ubuntu
  instance_type = "t3.micro"
  name          = "SV-VPN01"
  key_name      = "key_ssh"

  disk_type   = "gp2"
  disk_size   = 8
  disk_delete = true

  ports_tcp = {
    zabbix = "10050",
    vpn    = "13579",
  }

  cidr_blocks_tcp = ["172.31.32.0/20"]

  private_ip     = "172.31.32.56"
  private_subnet = "172.31.32.0/20"
}
